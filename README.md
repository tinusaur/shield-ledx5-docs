# Shield LEDx5 Docs

Shield LEDx5 Documentation.

## Getting started

- [Tinusaur Shield LEDx5 - Assembling](Tinusaur_Shield_LEDx5_Assembling_Guide.pdf)
- Download link at GitLab: https://gitlab.com/tinusaur/shield-ledx5-docs/-/raw/main/Tinusaur_Shield_LEDx5_Assembling_Guide.pdf?inline=false

